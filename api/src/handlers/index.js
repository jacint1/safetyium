module.exports = {
    handleRegisterUser: require('./handleRegisterUser'),
    handleAuthenticateUser: require('./handleAuthenticateUser'),
    handleRetrieveUser: require('./handleRetrieveUser'),
    handleUpdateUser: require('./handleUpdateUser'),
    handleIncidence: require('./handleIncidence'),
    handleIncidenceNearMe: require('./handleIncidenceNearMe'),
    handleComment: require('./handleComment')
}