const user = require('./user')
const reaction = require('./reaction')
const comment = require('./comment')
const incidence = require('./incidence')

module.exports = {
    user,
    reaction,
    comment,
    incidence
}