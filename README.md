## Safetyium

![](https://media.giphy.com/media/81xwEHX23zhvy/giphy.gif)

## Intro

Safetyium is an application that aims to keep you safe wherever you go by allowing you to ask for help and interact with other users at any time of possible danger.


## Funcional Description

- Landing
- Login y Register
- View current location
- View other users incidences
- Create incidences
- Incidences chat
- View chat history and interact

### Use Cases

![](./doc/images/Use_Cases.png)

## Flow Charts

Watch other users incidences

![](./doc/images/Users_Incidents.png)

Create Incidence

![](./doc/images/Create_incidence.png)

Incidence Chat

![](./doc/images/Incidence_Chat.png)


## Technical Description

### Blocks

![](./doc/images/blocks.png)

### Data Model

#### User
- id: (ObjectId)
- username: (String)
- name: (String)
- password: (String)

#### Message
- id (ObjectId)
- user (ObjectId)
- text (String)

#### Incidence

- id: (ObjectId)
- user: (ObjectId) userid
- altitude (String)
- longitude (String)
- description

## Technologies

- Javascript
- React
- CSS
- SASS
- HTML
- Express
- Mongo
- Node




